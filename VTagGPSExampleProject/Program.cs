﻿using PubSub;
using System;
using VTagGPSExampleProject.Libs;
using VTagGPSExampleProject.Libs.Messages;

namespace VTagGPSExampleProject
{
    public class Program
    {
        static DataCache dataCache;
        static void Main(string[] args)
        {
            Console.WriteLine("V-Tag GPS Example Project");
            //Subscribing to various messages
            Hub.Default.Subscribe<DailyReportMessage>(NewDailyReport);
            Hub.Default.Subscribe<NewTagMovementMessage>(NewTagMovement);
            Hub.Default.Subscribe<GatewayUpdateMessage>(GatewayUpdate);
            Hub.Default.Subscribe<GatewayRegisteredMessage>(GatewayRegistered);
            Hub.Default.Subscribe<VTagGpsRegisteredMessage>(VTagGpsRegistered);
            ChirpstackClient chirpstackClient = new ChirpstackClient();
            dataCache = new DataCache(chirpstackClient);
            ChirpstackApplicationWorker chirpstackApplicationWorker = new ChirpstackApplicationWorker(dataCache);
            chirpstackApplicationWorker.Start();
            ChirpstackRegisterWorker chirpstackRegisterWorker = new ChirpstackRegisterWorker(dataCache, chirpstackClient);
            dataCache.ChirpstackServerAddress = "https://vtaggpslr.infinidtech.com"; //our chirpstack cloud server. a custom on-prem one could be used if needed
            //username and password registered with the chirpstack server. InfinID generates these values and gives it to the client
            dataCache.ChirpstackUsername = "username";
            dataCache.ChirpstackPassword = "password";
            //here he would get a GPS Tag by its deviceId. the registerWorker will automatically register it if needed.
            //A VTag GPS tag will not be able to communicate with chirpStack until it is registered.
            //This DeviceID is fake.. a real one would need to be used.
            dataCache.GetVTagGps("008000000401DD79");
            //here we would add a gateway for the registerWorker to register.
            //A LoraWan Gateway will not be able to communicate with chirpStack until it is registered.
            //This GatewayID is fake.. a real one would need to be used.
            dataCache.Gateways.Add(new DataCache.Gateway("Multitech Conduit", "00800000a80012ea"));
            
            Console.ReadLine();
        }

        private static void VTagGpsRegistered(VTagGpsRegisteredMessage message)
        {
            Console.WriteLine(string.Format("VTagGpsRegistered:{0}", message.DeviceID));
        }

        private static void GatewayRegistered(GatewayRegisteredMessage message)
        {
            Console.WriteLine(string.Format("GatewayRegistered:{0}", message.GatewayID));
        }

        private static void GatewayUpdate(GatewayUpdateMessage message)
        {
            Console.WriteLine(string.Format("GatewayUpdate:GatewayId:{0},LastSeen:{1}",message.GatewayId, message.LastSeen));
        }

        private static void NewTagMovement(NewTagMovementMessage message)
        {
            Console.WriteLine(string.Format("NewTagMovement:{0}", message.VTagGps.ToString()));
        }

        private static void NewDailyReport(DailyReportMessage message)
        {
            Console.WriteLine(string.Format("DailyReport:{0}", message.VTagGps.ToString()));
        }
    }
}
