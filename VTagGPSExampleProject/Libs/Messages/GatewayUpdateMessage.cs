﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VTagGPSExampleProject.Libs.Messages
{
    public class GatewayUpdateMessage
    {
        public GatewayUpdateMessage(string gatewayId, DateTimeOffset? lastSeen)
        {
            GatewayId = gatewayId;
            LastSeen = lastSeen;
        }

        public string GatewayId { get; }
        public DateTimeOffset? LastSeen { get; }
    }
}
