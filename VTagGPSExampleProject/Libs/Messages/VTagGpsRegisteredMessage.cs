﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VTagGPSExampleProject.Libs.Messages
{
    public class VTagGpsRegisteredMessage
    {
        public VTagGpsRegisteredMessage(string deviceID)
        {
            DeviceID = deviceID;
        }

        public string DeviceID { get; }
    }
}
