﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VTagGPSExampleProject.Libs.Messages
{
    public class GatewayRegisteredMessage
    {
        public GatewayRegisteredMessage(string gatewayID)
        {
            GatewayID = gatewayID;
        }

        public string GatewayID { get; }
    }
}
