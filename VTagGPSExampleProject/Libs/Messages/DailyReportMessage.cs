﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VTagGPSExampleProject.Libs.Messages
{
    public class DailyReportMessage
    {
        public DailyReportMessage(VTagGps vTagGps)
        {
            VTagGps = vTagGps;
        }

        public VTagGps VTagGps { get; }
    }
}
