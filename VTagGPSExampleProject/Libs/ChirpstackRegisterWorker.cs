﻿using PubSub;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using VTagGPSExampleProject.Libs.Extensions;
using VTagGPSExampleProject.Libs.Messages;

namespace VTagGPSExampleProject.Libs
{
    public class ChirpstackRegisterWorker
    {
        public const int TIMERMS = 20000;
        public const int UPDATEGATEWAYSTIMEMS = 300000;
        public const string VTAGGPSAPPKEY = "9a12ffb5e015afc86a36ff06ca1187c5";//Application Key Registered under Chirpstack Server        
        public const string PROCESSORID = "VAGGPSPROCESSOR";
        private readonly DataCache dataCache;
        private readonly ChirpstackClient chirpstackClient;
        Timer gatewayTimer;
        Timer timer;
        public ChirpstackRegisterWorker(DataCache dataCache,
            ChirpstackClient chirpstackClient)
        {
            this.dataCache = dataCache;
            this.chirpstackClient = chirpstackClient;
        }

        public void Start()
        {
            Console.WriteLine("ChirpstackRegisterWorker.chirpstack worker.Start");
            timer = new Timer(new TimerCallback(DoWork), null, TIMERMS, TIMERMS);
            gatewayTimer = new Timer(new TimerCallback(GatewayTimerWorker), null, UPDATEGATEWAYSTIMEMS, UPDATEGATEWAYSTIMEMS);
        }

        public void Stop()
        {
        }

        public async void GatewayTimerWorker(Object state)
        {
            try
            {
                gatewayTimer.Change(Timeout.Infinite, Timeout.Infinite);
                if (!await dataCache.CheckChirpstackSetup())
                    return;
                List<ChirpstackClient.Gateway> gateways = await chirpstackClient.GetGateways(dataCache.OrganizationID);
                foreach (var gateway in gateways)
                {
                    string gatewayId = gateway.id;
                    DateTimeOffset? lastSeen = gateway.lastSeenAt;
                    Hub.Default.Publish<GatewayUpdateMessage>(new GatewayUpdateMessage(gatewayId, lastSeen));
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(string.Format("Got error in GatewayTimerWorker:DoWork-{0}", exc.Message));
            }
            finally
            {
                gatewayTimer.Change(UPDATEGATEWAYSTIMEMS, UPDATEGATEWAYSTIMEMS);
            }
        }


        public async void DoWork(Object state)
        {
            try
            {
                timer.Change(Timeout.Infinite, Timeout.Infinite);
                if (!await dataCache.CheckChirpstackSetup())
                {
                    Console.WriteLine("ChirpstackRegisterWorker.chirpstack not setup.");
                    return;
                }
                List<VTagGps> vTagGpsList = dataCache.GetUnregisteredVTagGps();
                foreach (var vTagGps in vTagGpsList)
                {
                    try
                    {
                        string deviceID = vTagGps.DeviceID?.ToLower();
                        if (string.IsNullOrWhiteSpace(deviceID))
                            continue;
                        ChirpstackClient.Device device = new ChirpstackClient.Device()
                        {
                            applicationID = dataCache.ApplicationID,
                            description = deviceID,
                            devEUI = deviceID,
                            deviceProfileID = dataCache.DeviceProfileID,
                            name = deviceID
                        };
                        await chirpstackClient.RegisterDevice(device, VTAGGPSAPPKEY);
                        vTagGps.Registered = true;
                        Hub.Default.Publish<VTagGpsRegisteredMessage>(new VTagGpsRegisteredMessage(deviceID));
                        //Update any database tables
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(string.Format("Error in registering VTag GPS with cloud server:{0}", exc.Message));
                    }
                }
                foreach (var reader in dataCache.Gateways)
                {
                    try
                    {
                        if (string.IsNullOrWhiteSpace(reader.GatewayID))
                            continue;
                        string gatewayId = reader.GatewayID;
                        try
                        {
                            await chirpstackClient.RegisterGateway(reader.Name, gatewayId, dataCache.OrganizationID);
                        }
                        catch (Exception exc)
                        {
                            if (!exc.Message.Contains("object already exists"))
                                throw exc;

                        }
                        reader.Registered = true;
                        Hub.Default.Publish<GatewayRegisteredMessage>(new GatewayRegisteredMessage(gatewayId));

                        //update any database tables
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(string.Format("Register Gateway {0} error {1}", reader.Name, exc.FullMessage()));
                    }
                }

            }
            catch (Exception exc)
            {
                Console.WriteLine(string.Format("Got error in ChirpstackRegisterWorker.DoWork-{0}", exc.Message));
            }
            finally
            {
                timer.Change(TIMERMS, TIMERMS);
            }
        }
    }
}
