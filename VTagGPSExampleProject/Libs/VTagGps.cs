﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VTagGPSExampleProject.Libs
{
    public class VTagGps
    {
        private double? latitude;
        public double? Latitude
        {
            get { return latitude; }
            set
            {
                if (value != null)
                    value = Math.Round(value.Value, 7);
                latitude = value;
            }
        }
        private double? longitude;
        public double? Longitude
        {
            get { return longitude; }
            set
            {
                if (value != null)
                    value = Math.Round(value.Value, 7);
                longitude = value;
            }
        }
        private double? altitude;
        public double? Altitude
        {
            get { return altitude; }
            set
            {
                if (value != null)
                    value = Math.Round(value.Value, 7);
                altitude = value;
            }
        }
        public int? NumberOfSatellites { get; set; }
        public DateTimeOffset? LastGPSFix { get; set; }
        public int? GPSAccuracy { get; set; }
        public string VTagType { get; set; }
        public string DeviceID { get; set; }
        public double? VTagX { get; set; }
        public double? VTagY { get; set; }
        public int? VTagZ { get; set; }
        public int MissedSatelliteFixes { get; set; } = 0;
        public double? BatteryLevel { get; set; }
        public DateTimeOffset? VTagLastSeen { get; set; }
        public DateTimeOffset? VTagLastMoved { get; set; }
        public bool Registered { get; set; } = false;
        public override string ToString()
        {
            return string.Format("Device ID:{0},Latitude:{1},Longitude:{2},NumberOfSatellites:{3},GPSAccuracy:{4}",DeviceID, Latitude, Longitude, NumberOfSatellites, GPSAccuracy);
        }
    }
}
