﻿using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Options;
using Newtonsoft.Json;
using PubSub;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VTagGPSExampleProject.Libs.Extensions;
using VTagGPSExampleProject.Libs.Messages;

namespace VTagGPSExampleProject.Libs
{
    /// <summary>
    /// Responsible for maintaining open MQTT connection to Chirpstack
    /// application server and processing messages to update DB
    /// </summary>
    public class ChirpstackApplicationWorker
    {
        public const int PROCESSTAGSTIMEMS = 30000;
        public const string PROCESSORID = "VAGGPSPROCESSOR";
        public string applicationID;
        private readonly DataCache dataCache;
        MqttFactory factory;
        IMqttClient mqttClient;
        int disconnectTimer = 0;
        Timer connectionTimer;
        List<ApplicationMessage> applicationMessages = new List<ApplicationMessage>();
        private SemaphoreSlim messageLock = new SemaphoreSlim(1, 1);

        public ChirpstackApplicationWorker(DataCache dataCache)
        {
            this.dataCache = dataCache;
        }

        public void Start()
        {
            try
            {
                factory = new MqttFactory();
                mqttClient = factory.CreateMqttClient();
                connectionTimer = new Timer(new TimerCallback(HandleConnectionProcessTags), null, PROCESSTAGSTIMEMS, PROCESSTAGSTIMEMS);
            }
            catch (Exception exc)
            {
                string message = exc.FullMessage();
                Console.WriteLine("ChirpstackAppWorker Start err " + message);
            }
        }

        public void Stop()
        {
        }

        public async Task HandleMessage(ApplicationMessage message)
        {
            //Parse the message. Extract latitude, longitude, numSatellites, accuracy
            byte[] messageBytes;
            double? latitude = null, longitude = null;
            int? numSatellites = null, accuracy = null;
            if (message.Data != null)
            {
                messageBytes = Convert.FromBase64String(message.Data);
                string messageHex = BitConverter.ToString(messageBytes).Replace("-", "");

                if (messageBytes.Length == 5)
                {
                    process_alert_message(message, messageBytes);
                    return;
                }

                VTagGps vTagGps = null;
                if (messageBytes.Length == 11)
                {
                    string deviceID = message.DevEUI;
                    //get vtaggps instance
                    vTagGps = dataCache.GetVTagGps(deviceID);
                    vTagGps.Registered = true;
                    byte indicatorByte = messageBytes[10];
                    bool gotFix = (indicatorByte & 0b01) == 1;
                    bool isMovement = (indicatorByte & 0b10) >> 1 == 1;
                    double? batteryVolts = decode_battery_level((indicatorByte >> 2) & 0x07);
                    if (gotFix)
                    {
                        byte[] latitudeBytes = new byte[4];
                        Array.Copy(messageBytes, 0, latitudeBytes, 0, 4);
                        int latitudeInt = BitConverter.ToInt32(latitudeBytes, 0);
                        latitude = Convert.ToDouble(latitudeInt);
                        latitude = latitude / 10000000;
                        byte[] longitudeBytes = new byte[4];
                        Array.Copy(messageBytes, 4, longitudeBytes, 0, 4);
                        int longitudeInt = BitConverter.ToInt32(longitudeBytes, 0);
                        longitude = Convert.ToDouble(longitudeInt);
                        longitude = longitude / 10000000;
                        byte numSatellitesByte = messageBytes[8];
                        numSatellites = (int)numSatellitesByte;
                        byte accuracyByte = messageBytes[9];
                        accuracy = (int)accuracyByte;
                        vTagGps.Latitude = latitude;
                        vTagGps.Longitude = longitude;
                        vTagGps.NumberOfSatellites = numSatellites;
                        vTagGps.GPSAccuracy = accuracy;
                        vTagGps.MissedSatelliteFixes = 0;
                        vTagGps.LastGPSFix = DateTimeOffset.Now;
                        vTagGps.BatteryLevel = batteryVolts;
                        if (isMovement)
                        {
                            // movement initiated report
                            vTagGps.VTagLastMoved = DateTimeOffset.Now;
                        }
                        else
                        {
                            // periodic report                         
                        }
                    }
                    else
                    {
                        // Communication but no fix
                        vTagGps.MissedSatelliteFixes++;
                        vTagGps.BatteryLevel = batteryVolts;
                        if (isMovement)
                        {
                            // movement initiated report
                            vTagGps.VTagLastMoved = DateTimeOffset.Now;
                        }
                        else
                        {
                            // periodic report                           
                        }
                    }
                    vTagGps.VTagLastSeen = DateTimeOffset.Now;
                    if (isMovement)
                        Hub.Default.Publish<NewTagMovementMessage>(new NewTagMovementMessage(vTagGps));
                    else
                        Hub.Default.Publish<DailyReportMessage>(new DailyReportMessage(vTagGps));
                    // Update location history
                    if (gotFix)
                    {
                        //Update any location history records
                    }
                    // Update reader stats. GatewayID is the unique id of the LoraWan Gateway registered with the Chirpstack server
                    string gatewayID = message.RxInfo.Length > 0 ? message.RxInfo[0].GatewayID : null;                    
                    if (gatewayID != null)
                    {
                        //here you can update any lastSeen field for the gateway record in the database 
                    }                                        
                }
            }
        }

        private async void process_alert_message(ApplicationMessage message, byte[] messageBytes)
        {
            string deviceID = message.DevEUI;
            // Alert type
            string alertType = "Unknown";
            if (messageBytes[2] == 0x01)
            {
                alertType = "High Temperature";
            }
            else if (messageBytes[2] == 0x02)
            {
                alertType = "Low Temperature";
            }
            else if (messageBytes[2] == 0x03)
            {
                alertType = "High and Low Temperature";
            }
            // Current temperature
            int temperature = BitConverter.ToInt16(messageBytes, 3);
            //here you can do proper alert that temperateure is out of range.            
        }

        // Battery level is sent in 3 bit integer
        public double? decode_battery_level(int code)
        {
            double? result = new double?();

            // Code 0 indicates no measurement available
            // Codes 1..7 indicate voltage in increments of 0.1
            if (code >= 1 && code <= 7)
            {
                result = 2.3 + code * 0.1;
            }
            return result;
        }

        public async void HandleConnectionProcessTags(Object state)
        {
            try
            {
                //Console.WriteLine("************AppWork.HandleConnectionProcessTags");
                //logger.LogInformation("***************ChirpstackApplicationWorker.HandleConnectionProcessTags Start");
                if (!await dataCache.CheckChirpstackSetup())
                {
                    Console.WriteLine("AppWork.HandleConnectionProcessTags.Chirp not setup");
                    return;
                }
                dataCache.ChirpstackConnected = mqttClient.IsConnected;
                if (!mqttClient.IsConnected)
                {

                    IMqttClientOptions options = new MqttClientOptionsBuilder()
                      .WithTcpServer(dataCache.MqttServerAddress, dataCache.ChirpstackPort)
                    .WithCredentials(dataCache.ChirpstackUsername, dataCache.ChirpstackPassword)
                    .WithKeepAliveSendInterval(TimeSpan.FromSeconds(10))
                    .WithCleanSession()
                    .Build();


                    mqttClient.UseDisconnectedHandler(async message =>
                    {
                        Console.WriteLine("**************************************Got Disconnect");
                        await mqttClient.UnsubscribeAsync(string.Format("application/{0}/#", dataCache.ApplicationID));
                        disconnectTimer++;
                    });
                    mqttClient.UseApplicationMessageReceivedHandler(async message =>
                    {
                        byte[] payload = message.ApplicationMessage.Payload;
                        string json = Encoding.UTF8.GetString(payload);
                        ApplicationMessage appMessage = JsonConvert.DeserializeObject<ApplicationMessage>(json);
                        string messageData = string.Empty;
                        if (appMessage.Data != null)
                        {
                            byte[] appMessageBytes = Convert.FromBase64String(appMessage.Data);
                            messageData = Encoding.UTF8.GetString(appMessageBytes);
                        }
                        //Console.WriteLine("***************Got message " + message.ApplicationMessage.Topic + ", data=" + messageData);
                        try
                        {
                            await messageLock.WaitAsync();
                            applicationMessages.Add(appMessage);
                        }
                        finally
                        {
                            messageLock.Release();
                        }
                    });
                    await mqttClient.ConnectAsync(options, CancellationToken.None);
                    Console.WriteLine("************MQTT Client connected?" + mqttClient.IsConnected);
                    await mqttClient.SubscribeAsync(string.Format("application/{0}/#", dataCache.ApplicationID));
                }
                try
                {
                    await messageLock.WaitAsync();
                    foreach (ApplicationMessage appMessage in applicationMessages)
                    {
                        await HandleMessage(appMessage);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Got error in Chirpstack Message Handling Routine");
                }
                finally
                {
                    applicationMessages.Clear();
                    messageLock.Release();
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Got error in Chirpstack Handle Connection Messages");
            }
            finally
            {
                //logger.LogInformation("***************ChirpstackApplicationWorker.HandleConnectionProcessTags DONE");
                connectionTimer.Change(PROCESSTAGSTIMEMS, PROCESSTAGSTIMEMS);
            }
        }//DoWork

        public class ApplicationMessage
        {
            public string ApplicationID { get; set; }
            public string ApplicationName { get; set; }
            public string DeviceName { get; set; }
            public string DevEUI { get; set; }
            public string Data { get; set; }
            public RxInformation[] RxInfo { get; set; }

            public class RxInformation
            {
                public string GatewayID { get; set; }
                public string Name { get; set; }
                public string Rssi { get; set; }
                public string LoraSNR { get; set; }
                public GPSLocation Location { get; set; }

                public class GPSLocation
                {
                    public decimal? Latitude { get; set; }
                    public decimal? Longitude { get; set; }
                    public int? Altitude { get; set; }
                }
            }
        }

    }
}
