﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace VTagGPSExampleProject.Libs
{
    public class DataCache
    {
        public string ApplicationID { get; set; }
        private string serviceProfileID;
        internal string DeviceProfileID { get; set; }
        internal string OrganizationID { get; set; }
        private string networkId = "1";
        public string MqttServerAddress { get; set; }
        private readonly ChirpstackClient chirpstackClient;
        public bool ChirpstackConnected = false;
        public string ChirpstackServerAddress { get; set; } = "https://vtaggpslr.infinidtech.com";
        public int ChirpstackPort { get; set; } = 8883;
        public string ChirpstackUsername { get; set; } = "chirpstack username provided by infinid";
        public string ChirpstackPassword { get; set; } = "chirpstack password provided by infinid";
        public DataCache(ChirpstackClient chirpstackClient)
        {
            this.chirpstackClient = chirpstackClient;
        }

        #region Semaphore
        private SemaphoreSlim semaphore = new SemaphoreSlim(1, 1);
        public int LockCount()
        {
            return semaphore.CurrentCount;
        }

        public async Task Lock([CallerMemberName] string propertyName = "", [CallerFilePath] string callerFilePath = "")
        {
            await this.semaphore.WaitAsync();
        }

        public void ReleaseLock([CallerMemberName] string propertyName = "", [CallerFilePath] string callerFilePath = "")
        {
            semaphore.Release();
        }

        #endregion Semaphore

        #region Chirpstack 


        public void ResetChirpstack()
        {
            ApplicationID = null;
            serviceProfileID = null;
            DeviceProfileID = null;
            OrganizationID = null;
        }

        public async Task<bool> CheckChirpstackSetup()
        {
            //First, do we have the necessary credentials to make chirpstack api calls?
            try
            {
                await this.Lock();
                if (string.IsNullOrWhiteSpace(ApplicationID)
                    || string.IsNullOrWhiteSpace(serviceProfileID)
                    || string.IsNullOrWhiteSpace(DeviceProfileID)
                    || string.IsNullOrWhiteSpace(OrganizationID))
                {
                    MqttServerAddress = ChirpstackServerAddress.Replace("https://", "").Replace("http://", "");
                    //update chirpstackclient to use latest appSettings server credentials
                    this.chirpstackClient.SetServiceOptions(new ChirpstackClient.ServiceOptions() { BaseUrl = ChirpstackServerAddress, Username = ChirpstackUsername, Password = ChirpstackPassword });
                    var organization = await chirpstackClient.GetOrganization();
                    OrganizationID = organization.id;
                    var serviceProfile = await chirpstackClient.GetServiceProfile(OrganizationID);
                    serviceProfileID = serviceProfile.id;
                    var deviceProfile = await chirpstackClient.GetDeviceProfile(OrganizationID);
                    DeviceProfileID = deviceProfile.id;
                    var application = await chirpstackClient.GetApplication(OrganizationID, serviceProfileID);
                    ApplicationID = application.id;
                    return true;
                }
                return true;
            }
            catch (Exception exc)
            {
                return false;
            }
            finally
            {
                //logger.LogDebug("CheckChirpstackSetup releasing lock.");
                this.ReleaseLock();
            }
        }

        #endregion Chirpstack

        #region VTagGPS

        private List<VTagGps> allVTagGps = new List<VTagGps>();
        /// <summary>
        /// Returns matching VTagGps instance or creates a new one if not found
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public VTagGps GetVTagGps(string deviceId)
        {
            VTagGps vTagGps = allVTagGps.FirstOrDefault(p => p.DeviceID == deviceId);            
            if (vTagGps == null)
            {
                vTagGps = new VTagGps() { DeviceID = deviceId };
                allVTagGps.Add(vTagGps);
            }
            return vTagGps;
        }

        public List<VTagGps> GetUnregisteredVTagGps()
        {
            return allVTagGps.Where(p => p.Registered == false).ToList();
        }

        #endregion VTagGPS

        public List<Gateway> Gateways { get; set; } = new List<Gateway>();

        public List<Gateway> GetUnregisteredGateways()
        {
            return Gateways.Where(p => p.Registered == false).ToList();
        }

        public class Gateway
        {
            public Gateway(string name, string gatewayID)
            {
                Name = name;
                GatewayID = gatewayID;
            }
            public bool Registered { get; set; } = false;
            public string Name { get; }
            public string GatewayID { get; }
        }
    }
}
