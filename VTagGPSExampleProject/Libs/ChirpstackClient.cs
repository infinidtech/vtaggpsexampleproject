﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace VTagGPSExampleProject.Libs
{
    public class ChirpstackClient
    {
        protected ServiceOptions serviceOptions;
        protected AuthToken authToken = new AuthToken();
        public ChirpstackClient()
        {
        }

        public void SetServiceOptions(ServiceOptions serviceOptions)
        {
            this.serviceOptions = serviceOptions;
            if (serviceOptions.BaseUrl.IndexOf("http") == -1)
                serviceOptions.BaseUrl = "http://" + serviceOptions.BaseUrl + ":8080";
            if (!serviceOptions.BaseUrl.Contains("/api"))
            {
                serviceOptions.BaseUrl = serviceOptions.BaseUrl + "/api/";
            }
            this.serviceOptions = serviceOptions;
            this.authToken.tokenExpires = null;
            this.authToken.token = null;
        }

        public async Task RegisterDevice(Device device, string nwkKey)
        {
            //first check if device is already registered...
            Device existing = await GetDevice(device.devEUI);
            if (existing != null)
                return;
            //first register device
            await DoPost<Gateway>("devices", null, new { device });
            DeviceKey deviceKey = new DeviceKey() { devEUI = device.devEUI, nwkKey = nwkKey };
            await DoPost<DeviceKey>("devices", device.devEUI + "/keys", new { deviceKeys = deviceKey });
        }

        public async Task<List<Gateway>> GetGateways(string organizationID)
        {
            var gatewaysResult = await DoGet<GatewaysResult>("gateways", "",
                new QueryParam("limit", 1),
                new QueryParam("offset", "0"),
                new QueryParam("organizationID", organizationID));
            return gatewaysResult.result.ToList();
        }

        public async Task RegisterGateway(string name, string gatewayId, string organizationID)
        {
            Gateway existing = await GetGateway(gatewayId);
            if (existing != null)
                return;
            Gateway gateway = new Gateway()
            {
                id = gatewayId,
                name = name.Replace(" ", "").ToLower(),
                description = name,
                organizationID = organizationID,
                location = new Gateway.Location() { altitude = 500 }
            };

            await DoPost<Gateway>("gateways", "", new { gateway });
        }

        public async Task<Application> GetApplication(string organizationID, string serviceProfileID)
        {
            ApplicationsResult applicationsResult = await DoGet<ApplicationsResult>("applications",
                "", new QueryParam("limit", 1), new QueryParam("organizationID", organizationID));
            if (applicationsResult.result.Length == 0)
            {
                Application application = new Application()
                {
                    name = "VTagGPS",
                    description = "VTagGPS",
                    organizationID = organizationID,
                    serviceProfileID = serviceProfileID
                };
                object wrapper = new { application };
                await DoPost<DeviceProfile>("applications", "", wrapper);
                applicationsResult = await DoGet<ApplicationsResult>("applications", "", new QueryParam("limit", 1), new QueryParam("organizationID", organizationID));
            }
            return applicationsResult.result.Length > 0 ? applicationsResult.result[0] : null;
        }

        public async Task<Organization> GetOrganization()
        {
            OrganizationsResult organizationsResult = await DoGet<OrganizationsResult>("organizations", "", new QueryParam("limit", "1"), new QueryParam("offset", "0"));
            return organizationsResult.result.Length > 0 ? organizationsResult.result[0] : null;
        }

        public async Task<ServiceProfile> GetServiceProfile(string organizationID)
        {
            ServiceProfilesResult serviceProfilesResult = await DoGet<ServiceProfilesResult>("service-profiles",
                "",
                new QueryParam("limit", 1),
                new QueryParam("offset", "0"),
                new QueryParam("organizationID", organizationID));
            return serviceProfilesResult.result.Length > 0 ? serviceProfilesResult.result[0] : null;
        }

        public async Task<Device> GetDevice(string deviceID)
        {
            try
            {
                DeviceResult deviceResult = await DoGet<DeviceResult>("devices", deviceID);
                return deviceResult.device;
            }
            catch (Exception exc)
            {
                return null;
            }
        }

        public async Task<Gateway> GetGateway(string gatewayID)
        {
            try
            {
                GatewayResult gatewayResult = await DoGet<GatewayResult>("gateways", gatewayID);
                return gatewayResult.gateway;
            }
            catch (Exception exc)
            {
                return null;
            }
        }


        public async Task<DeviceProfile> GetDeviceProfile(string organizationID)
        {
            DeviceProfilesResult deviceProfiles = await DoGet<DeviceProfilesResult>("device-profiles",
               "", new QueryParam("limit", 1), new QueryParam("organizationID", organizationID));
            if (deviceProfiles.result.Length == 0)
            {
                DeviceProfile deviceProfile = new DeviceProfile() { name = "VTagGPS", organizationID = organizationID };
                object deviceProfileWrapper = new { deviceProfile };
                await DoPost<DeviceProfile>("device-profiles", "", deviceProfileWrapper);
                deviceProfiles = await DoGet<DeviceProfilesResult>("device-profiles", "", new QueryParam("limit", 1), new QueryParam("organizationID", organizationID));
            }
            return deviceProfiles.result.Length > 0 ? deviceProfiles.result[0] : null;
        }

        private async Task<GatewayProfile> GetGatewayProfileID()
        {

            List<GatewayProfile> gatewayProfiles = await DoGet<List<GatewayProfile>>("gateway-profiles",
               "", new QueryParam("limit", 1));
            if (gatewayProfiles == null || gatewayProfiles.Count == 0)
            {
                GatewayProfile gatewayProfile = new GatewayProfile() { name = "VTagGPSGateway", networkServerID = "1", networkServerName = "Network Server" };
                object deviceProfileWrapper = new { gatewayProfile };
                await DoPost<GatewayProfile>("device-profiles", "", deviceProfileWrapper);
                gatewayProfiles = await DoGet<List<GatewayProfile>>("gateway-profiles", "", new QueryParam("limit", 1));
            }
            return gatewayProfiles.Count > 0 ? gatewayProfiles[0] : null;
        }

        #region Http Call Helpers

        async Task<S> DoHttp<S>(Method method, string resource, string action, object body, params QueryParam[] parameters)
        {
            var content = await GetHttpContent(method, resource, action, body, parameters);
            string json = await content.ReadAsStringAsync();
            if (!string.IsNullOrWhiteSpace(json))
            {
                S result = JsonConvert.DeserializeObject<S>(json);
                return result;
            }
            return default(S);
        }

        async Task<S> DoGet<S>(string resource, string action, params QueryParam[] parameters) where S : new()
        {
            return await DoHttp<S>(Method.GET, resource, action, null, parameters);
        }

        async Task<S> DoPost<S>(string resource, string action, object body, params QueryParam[] parameters)
        {
            return await DoHttp<S>(Method.POST, resource, action, body, parameters);
        }

        async Task<S> DoPut<S>(string resource, string action, object body, params QueryParam[] parameters)
        {
            return await DoHttp<S>(Method.PUT, resource, action, body == null ? "" : body, parameters);
        }

        #endregion Http Call Helpers

        protected async Task<HttpContent> GetHttpContent(Method method, string resource, string action, object body, params QueryParam[] parameters)
        {
            await GetToken();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(GetBaseUrl());
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("Grpc-Metadata-Authorization", string.Format("Bearer {0}", authToken.token));
                    HttpResponseMessage response = null;
                    string parms = String.Join("&", parameters.Select(p => p.Name + "=" + p.Value));
                    string actionBit = string.IsNullOrWhiteSpace(action) ? "" : "/" + action;
                    string reqUri = parms.Length > 0 ? resource + actionBit + "?" + parms : resource + actionBit;
                    string json = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine("About to make WS Call for API " + resource + "/" + action + " " + DateTime.Now.ToString("HH:mm:ss:fff"));
                    switch (method)
                    {
                        case Method.GET:
                            response = await client.GetAsync(reqUri);
                            break;
                        case Method.POST:
                            response = await client.PostAsync(reqUri, new StringContent(json, Encoding.UTF8, "application/json")).ConfigureAwait(false);
                            break;
                        case Method.PUT:
                            response = await client.PutAsync(reqUri, new StringContent(json, Encoding.UTF8, "application/json")).ConfigureAwait(false);
                            break;
                        case Method.DELETE:
                            response = await client.DeleteAsync(reqUri);
                            break;
                    }
                    if (response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.NoContent)
                    {
                        return response.Content;
                    }
                    else
                    {
                        throw await HandleBadResponse(response, resource, action);
                    }
                }
            }
            catch (Exception exc)
            {
                string message = exc.InnerException == null ? exc.Message : exc.Message + Environment.NewLine + exc.InnerException.Message;
                System.Diagnostics.Debug.WriteLine("Got Error:" + message);
                //go ahead and reset the token just in case token timeout..
                authToken.token = null;
                throw new Exception(message);
            }
        }

        protected async Task<string> DoHttp(Method method, string resource, string action, object body, params QueryParam[] parameters)
        {
            var content = await GetHttpContent(method, resource, action, body, parameters);
            string json = await content.ReadAsStringAsync();
            return json;
        }

        protected async Task<string> DoGet(string resource, string action, params QueryParam[] parameters)
        {
            return await DoHttp(Method.GET, resource, action, null, parameters);
        }

        protected async Task<string> DoPost(string resource, string action, object body, params QueryParam[] parameters)
        {
            return await DoHttp(Method.POST, resource, action, body, parameters);
        }

        protected async Task DoDelete(string resource, string action, object body = null, params QueryParam[] parameters)
        {
            await DoHttp(Method.DELETE, resource, action, body == null ? "" : body, parameters);
        }

        protected async Task<string> DoPut(string resource, string action, object body, params QueryParam[] parameters)
        {
            return await DoHttp(Method.PUT, resource, action, body == null ? "" : body, parameters);
        }

        #region Token Helpers

        protected bool TokenExpired
        {
            get
            {
                //subtract 10 seconds from expiration date just to be safe for latency times, etc.
                if (!authToken.tokenExpires.HasValue || DateTime.Now >= authToken.tokenExpires.Value.AddSeconds(-10))
                    return true;
                return false;
            }
        }

        public bool HasToken
        {
            get
            {
                return !string.IsNullOrWhiteSpace(authToken.token);
            }
        }

        protected string GetBaseUrl()
        {
            if (!serviceOptions.BaseUrl.Contains("/api"))
                return serviceOptions.BaseUrl + "/api/";
            else
                return serviceOptions.BaseUrl;
        }

        public async Task<bool> GetToken([CallerMemberName] string propertyName = "")
        {
            await authToken.Semaphore.WaitAsync();
            if (HasToken)
            {
                authToken.Semaphore.Release();
                return true;
            }
            string baseUrl = GetBaseUrl();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(baseUrl);
                    HttpContent body = new StringContent("username=" + serviceOptions.Username + "&password=" + serviceOptions.Password + "&isUser=" + serviceOptions.IsUser);
                    string json = JsonConvert.SerializeObject(new LoginAttempt() { username = serviceOptions.Username, password = serviceOptions.Password });
                    body = new StringContent(json, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await client.PostAsync("internal/login", body);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string returnjson = await response.Content.ReadAsStringAsync();
                            if (!string.IsNullOrWhiteSpace(returnjson))
                            {
                                MyToken myToken = JsonConvert.DeserializeObject<MyToken>(returnjson);
                                authToken.token = myToken.jwt;
                                return true;
                            }
                            else
                                return false;
                        }
                    }
                    authToken.token = null;
                    authToken.tokenExpires = null;
                    Exception exc = await HandleBadResponse(response, "token", "");
                    throw exc;
                }
            }
            catch (Exception exc)
            {
                string message = exc.InnerException == null ? exc.Message : exc.Message + Environment.NewLine + exc.InnerException.Message;

                throw new Exception(message);
            }
            finally
            {
                authToken.Semaphore.Release();
            }
        }

        protected async Task<Exception> HandleBadResponse(HttpResponseMessage resp, string resource, string action)
        {
            string content = await resp.Content.ReadAsStringAsync();
            return new Exception(content);
        }

        protected class MyToken
        {
            public string jwt { get; set; }
        }

        protected class QueryParam
        {
            public readonly string Name, Value;
            public QueryParam(string name, object value)
            {
                this.Name = name;
                string val = value == null ? "" : value.ToString();
                this.Value = WebUtility.UrlEncode(val);
            }
        }


        #endregion Token Helpers

        public class ServiceOptions
        {
            public string BaseUrl { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public bool IsUser { get; set; } = false;
        }

        public class AuthToken
        {
            public AuthToken()
            {
                ID = Guid.NewGuid().ToString("N");
            }
            public string ID { get; }
            public string token { get; set; }
            public DateTime? tokenExpires { get; set; }
            public SemaphoreSlim Semaphore = new SemaphoreSlim(1, 1);
        }

        public class ApplicationsResult
        {
            public Application[] result { get; set; }
            public string totalCount { get; set; }
        }

        public class Application
        {
            public string description { get; set; }
            public string id { get; set; }
            public string name { get; set; }
            public string organizationID { get; set; }
            public string serviceProfileID { get; set; }
            public string serviceProfileName { get; set; }
        }

        public class GatewayProfile
        {
            public DateTime createdAt { get; set; }
            public string id { get; set; }
            public string name { get; set; }
            public string networkServerID { get; set; }
            public string networkServerName { get; set; }
            public DateTime updatedAt { get; set; }
        }

        //This will be posted to /api/gateways
        public class Gateway
        {
            public Location location { get; set; }
            public string name { get; set; }
            public string description { get; set; }
            public string id { get; set; }
            public string gatewayProfileID { get; set; } = "34930a64-d453-4d44-8b85-3f824cf6dfd5";
            public string networkServerID { get; set; } = "1";
            public string organizationID { get; set; }
            public DateTimeOffset? lastSeenAt { get; set; }

            public class Location
            {
                public int altitude { get; set; }
            }
        }

        public class DeviceResult
        {
            public Device device { get; set; }
        }

        public class GatewaysResult
        {
            public Gateway[] result { get; set; }
            public string totalCount { get; set; }
        }

        public class GatewayResult
        {
            public DateTime? createdAt { get; set; }
            public DateTime? firstSeenAt { get; set; }
            public Gateway gateway { get; set; }
            public DateTime? lastSeenAt { get; set; }
            public DateTime? updatedAt { get; set; }
        }

        //This will be posted to /api/devices
        public class Device
        {
            public string applicationID { get; set; }
            public string description { get; set; }
            public string devEUI { get; set; }
            public string deviceProfileID { get; set; }
            public string name { get; set; }
            public int referenceAltitude { get; set; } = 0;
            public bool skipFCntCheck { get; set; } = true;

        }

        //post in format {deviceProfile:myDeviceProfile} to /api/device-profiles
        public class DeviceProfile
        {
            public string name { get; set; }
            public string networkServerID { get; set; } = "1";
            public string macVersion { get; set; } = "1.0.3";
            public string regParamsRevision { get; set; } = "A";
            public bool supportsJoin { get; set; } = true;
            public string organizationID { get; set; }
            public string id { get; set; }
        }

        public class DeviceProfilesResult
        {
            public DeviceProfile[] result { get; set; }
            public string totalCount { get; set; }
        }

        public class OrganizationsResult
        {
            public Organization[] result { get; set; }
            public string totalCount { get; set; }
        }

        //Need organizationID for some other entities try GET /api/organizations
        public class Organization
        {
            public bool canHaveGateways { get; set; }
            public DateTime createdAt { get; set; }
            public string displayName { get; set; }
            public string id { get; set; }
            public string name { get; set; }
            public DateTime updatedAt { get; set; }
        }

        // Posted like {serviceProfile:myServiceProfile} to /api/service-profiles
        public class ServiceProfile
        {
            public string name { get; set; }
            public string networkServerID { get; set; } = "1";
            public bool addGWMetaData { get; set; }
            public string organizationID { get; set; }
            public string id { get; set; }
        }

        public class ServiceProfilesResult
        {
            public ServiceProfile[] result { get; set; }
            public string totalCount { get; set; }
        }

        //This will be posted to /api/devices/{device_keys.dev_eui}/keys
        public class DeviceKey
        {
            //public string appKey { get; set; }
            public string devEUI { get; set; } //unique device id
            public string nwkKey { get; set; } //this is the appKey
        }

        public class LoginAttempt
        {
            public string username { get; set; }
            public string password { get; set; }
        }

        public enum Method { GET, PUT, POST, DELETE };
    }
}
